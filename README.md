# chaiwala

# Security concerns:

#SSL certificate MUST be used:
Any website which gets data from user must receive data over https instead of http. This is not part of coding, so this code does not address this issue.

#2. Cross site request forgery (CSRF):
The biggest concern is cross site request forgery (CSRF). This code handles this concern through session based token. So, CSRF is handled here.

#3. Properly escaping html/script tags:
Any submitted code which has html or javascript code must be properly escaped. It should treat the input script code (if any) as text instead of code

#4. SQL Injection attacks:
It is not specified whether the input data from textarea is to be stored in a database or not. Normally, the data is stored. In this case, it MUST be properly handled to avoid sql injection attacks. As there is no such requirement in this code, so I've not handled SQL injection related issues here, but it is a MUST in live server environments where storing and retrieving data to and from database is involved.

#5. Character set entered in the text area:
It is not specified what character set the textarea is expecting. This is not related to security, but it should be mentioned. My code handles ASCII along with commonly used languages like German, Spanish, French etc., but if Chinese, Japanese etc. etc. languages are to be handled, complexity will increase and code needed to be modified according to the scenario.

Thats it!
