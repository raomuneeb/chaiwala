<?php

    session_start();
    if (empty($_SESSION['token'])) {
        if (function_exists('mcrypt_create_iv')) {
            $_SESSION['token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
        } else {
            $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
        }
    }
    $token = $_SESSION['token'];

    if (!empty($_POST)){
        if (!empty($_POST['token'])) {
            if (hash_equals($_SESSION['token'], $_POST['token'])) {
                if(isset($_POST['case']) && $_POST['case'] == 'yes')
                    $strInput = trim(htmlspecialchars($_POST['assignment']));
                else
                    $strInput = trim(strtolower(htmlspecialchars($_POST['assignment'])));

                $pattern = '/[\s,:?!]+/u';
                $data = preg_split($pattern, $strInput, -1, PREG_SPLIT_NO_EMPTY);
                $data = array_count_values($data);

                arsort($data);
                echo "<p><strong>Total number of input words:</strong> " . count($data) . "</p>";
?>
                <table>
                    <tr>
                        <th align="left">Word</th>
                        <th>Count</th>
                    </tr>
                    <?php foreach($data as $word=>$count): ?>
                        <tr>
                            <td><?php echo $word; ?></td>
                            <td align="right"><?php echo $count; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <br /><br /> <strong>Input was:</strong> <br /><br /> <?=nl2br($strInput);?>
<?php
            }else{
                echo 'token not matched';
            }
        }
    }
?>

<html>
<head>

</head>
<body>
<br /><br />


<form method="post" action="">
    <label for="case">Case Sensitive?</label>
    <?php
        if(isset($_POST['case']) && $_POST['case'] == 'yes')
            echo '<input type="checkbox" name="case" id="case" value="yes" checked><br>';
        else
            echo '<input type="checkbox" name="case" id="case" value="yes"><br>';

    ?>


    <textarea name="assignment" rows="10" cols="50"></textarea>
    <input type="hidden" name="token" value="<?=$token?>"
    <br />

<br >
    <input type="submit" value="Submit">
</form>
</body>
</html>

